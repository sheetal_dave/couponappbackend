const {db} = require('../util/firebase-admin');

exports.getAllCoupons = async (req, res) => {
    let coupons = [];
    const snapshots = await db.collection('coupon').get();
    snapshots.forEach(doc => {
        coupons.push({
            id: doc.id,
            logo: doc.data().logo,
            title: doc.data().title,
            description: doc.data().description,
            code: doc.data().code,
            unlock: doc.data().unlock
        });
    });
    if (!coupons.length) {
        return res.status(204).json({status: 'success', data: []});
    }
    return res.status(200).json({status: 'success', data: coupons});
}

exports.unLockCoupon = async (req, res) => {

    const doc = await db.doc(`/coupon/${req.params.id}`).get();

    if (!doc.exists) {
        return res.status(404).json({error: 'Coupon not found'});
    }

    doc.ref.update({unlock: true});
    return res.status(200).json({status: 'success'});
}