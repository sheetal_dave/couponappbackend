const functions = require('firebase-functions');

const app = require('express')();

const cors = require('cors');
app.use(cors());

const {getAllCoupons, unLockCoupon} = require('./handler/coupon');

app.get('/coupons', getAllCoupons);
app.put('/unlock-coupon/:id', unLockCoupon);

exports.api = functions.region('europe-central2').https.onRequest(app);